(ns instacomb.util
  (:require [instaparse.core :as ip])
  (:require [instaparse.combinators :as ic]))

(defn keys-in [m]
  (if (or (not (map? m))
          (empty? m))
    '(())
    (for [[k v] m
          subkey (keys-in v)]
      (cons k subkey))))

(defn grammar->parser [p]
  "extract the very rule from a grammar-map; for example, from
     {:grammar
      {:G__7195
       {:red {:reduction-type :hiccup, :key :G__7195},
        :tag :opt,
        :parser {:string 'a', :tag :string, :hide nil}}},
     :start-production :G__7195,
     :output-format :hiccup}
   results
     {:tag :opt, :parser {:string 'a', :tag :string, :hide nil}}"
  (dissoc ((:start-production p) (:grammar p)) :red))

(defn terminal? [p]
  "checks, wether p recognises a  terminal symbol erkennt; 
   strings and regular expressions are terminals"
  (not (some #(= % :non-terminal) (flatten (keys-in (:grammar p))))))

(defn non-terminal [p]
  "yields 
      - the parser p, if p recognises a non-terminal
      - nil otherwise"
  (if-not (terminal? p) p))

(defn t-or-nt [p]
  "yields the very rule of p, if p recognises a terminal
   otherwise a :nt-rule"
  (if (terminal? p)
    (merge {:hide (:hide p)} (grammar->parser p))
    {:hide (:hide p) :tag :nt :keyword (:start-production p)}))

(defn combinator-function
  "takes an instaparse combinator keyword of a unary combinator and 
  creates a function, which takes a parser p and yields
  a parser for the combinator keyword"
  [combinator-keyword]
  (fn 
    [p]
    (let [rule-name (keyword (gensym))] 
      (ip/parser
       (assoc
        (if (terminal? p) {} (:grammar p))
        rule-name
        {:tag combinator-keyword,
         :parser (t-or-nt p)})
       :start rule-name))))

(defn combinator-function*
  "takes an instaparse combinator keyword of a unary combinator and 
  creates a function, which takes a rule-name and a parser p and yields
  a parser for the combinator keyword"
  [combinator-keyword]
  (fn 
    [rule-name p]
    (ip/parser (assoc
                (if (terminal? p) {} (:grammar p))
                rule-name
                {:tag combinator-keyword,
                 :parser (t-or-nt p)})
               :start rule-name)))

(defn combinator-function-n
  "takes an instaparse combinator keyword of a n-ary combinator and 
  creates a function, which takes a parser p and yields
  a parser for the combinator keyword"
  [combinator-keyword]
  (fn 
    [p1 p2 & parsers]
    (let [rule-name (keyword (gensym))]
      (ip/parser
       (assoc (merge
               (:grammar (non-terminal p1)) (:grammar (non-terminal p2))
               (apply merge (map (fn [p] (:grammar (non-terminal p))) parsers)))
              rule-name
              {:tag combinator-keyword,
               :parsers 
               (concat (list (t-or-nt p1)
                             (t-or-nt p2))
                       (map t-or-nt parsers))}) ;
       :start rule-name))))

(defn combinator-function-n*
  "takes an instaparse combinator keyword of a n-ary combinator and 
  creates a function, which takes a rule-name and a parser p and yields
  a parser for the combinator keyword"
  [combinator-keyword]
  (fn 
    [rule-name p1 p2 & parsers]
    (ip/parser
     (assoc (merge
             (:grammar (non-terminal p1)) (:grammar (non-terminal p2))
             (apply merge (map (fn [p] (:grammar (non-terminal p))) parsers)))
       rule-name
       {:tag combinator-keyword,
        :parsers 
        (concat (list (t-or-nt p1)
                      (t-or-nt p2))
                (map t-or-nt parsers))}) ;
     :start rule-name)))

;;;;;
(defn combinator-function->grammarmap-function
  "converts combinator funcion s into a grammar-map function
  or encloses s in a nt grammar-map function"
  [s]
  (cond
    (contains? (ns-interns 'instaparse.combinators) s)
    (symbol "instaparse.combinators" (name s))
    :else (list 'instaparse.combinators/nt (keyword s))))

(defn convert-grammar 
  "converts instacomb combinator function calls into instaparse
  combinator function calls"
  [exp]
  (cond
    (empty? exp) ()
    (seq? (first exp))
    (cons (convert-grammar (first exp)) (convert-grammar (rest exp)))
    (not (symbol? (first exp)))
    (cons (first exp) (convert-grammar (rest exp)))
    :else (cons (combinator-function->grammarmap-function
                 (first exp)) (convert-grammar (rest exp)))))

(defmacro combinator-expression->grammarmap [vec]
  "converts an instacomb expression into an instaparse combinator 
   expression"
  (if (empty? vec)
    {}
    (let [[sy gr & v] vec]
      `(merge {(keyword '~sy) (convert-grammar '~gr)}
              (combinator-expression->grammarmap ~v)))))




  

