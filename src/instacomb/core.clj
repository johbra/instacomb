(ns instacomb.core
  (:require [instaparse.core :as ip])
  (:require [instaparse.combinators :as ic])
  (:require [instacomb.util :as u]))


;; primitive parsers for strings, regular expressions, and epsilon

(defn string
  "create a parser for string str"
  [str]
  (let [rule-name (keyword (gensym))]
    (ip/parser {rule-name  (ic/string str)}
               :start rule-name)))

(defn string*
  "create a parser for string str"
  [rule-name str]
  (ip/parser {rule-name  (ic/string str)} 
             :start rule-name))

(defn regexp
  "create a parser for regexpr re"
  [re]
  (let [rule-name (keyword (gensym))] 
    (ip/parser {rule-name  (ic/regexp re)} 
               :start rule-name)))

(defn regexp*
  "create a parser for regexpr re"
  [rule-name re]
  (ip/parser {rule-name  (ic/regexp re)} 
             :start rule-name))

(defn epsilon
  "create epsilon-parser"
  []
  (ip/parser {:epsilon ic/Epsilon}
             :start :epsilon))

;; basic combinator functions

(def alt (u/combinator-function-n :alt))
(def alt* (u/combinator-function-n* :alt))
(def cat (u/combinator-function-n :cat))
(def cat* (u/combinator-function-n* :cat))
(def opt (u/combinator-function :opt))
(def opt* (u/combinator-function* :opt))
(def star (u/combinator-function :star))
(def star* (u/combinator-function* :star))
(def plus (u/combinator-function :plus))
(def plus* (u/combinator-function* :plus))
(def look (u/combinator-function :look))
(def look* (u/combinator-function* :look))

;; ... for nesting parse trees
(defn nt
  "mark parser p as non-terminal"
  [p]
  (let [rule-name (keyword (gensym))] 
    (assoc-in p [:grammar (:start-production p) :non-terminal] true)))


;; ... for auto-whitespace
(defn auto-whitespace [p whitespace-parser]
  "activates auto-whitespace-functionality of instaparse"
  (ip/parser (:grammar p)
             :start (:start-production p)
             :auto-whitespace whitespace-parser))

;; macro for recursive rules
(defmacro parser-comb
  "takes a symbol and a vector of rules and converts them into a 
  instaparse grammar-map"
  [sym rules]
  `(let [gr# (u/combinator-expression->grammarmap ~rules)]
     (nt (ip/parser (eval gr#)  :start (keyword '~sym)))))

;; hiding 
(defn hide
  "removes content from the parse tree"
  [p] (assoc p :hide true))

;; Hiding the names of subrules in parse trees is the default
;; behavior of instacomb. If you want the names of subrules appear,
;; use nt. You need hide-tag only if the name of the outermost rule
;; should dissappear
(defn hide-tag
  "hides the rule name of p in parse tree"
  [p]
  (let [g (:grammar p)]
    (assoc-in p [:grammar (first (keys g)) :red] {:reduction-type :raw})))








 
