(ns instacomb.core-test
  (:require [clojure.test :refer :all]
            [instacomb.core :refer :all]
            [instaparse.core :as ip]
            [instaparse.combinators :as ic]))

(defn symstr?
  [v  & str]
  (and (= (type (first v)) clojure.lang.Keyword)
       (or (empty? (rest v)) (= (rest v) str))))

(deftest primitive-parser-tests
  (testing "string parsers"
    (is (symstr? ((string "abc") "abc") "abc"))
    (is (= ((string* :abc "abc") "abc") [:abc "abc"]))
    (is (ip/failure? ((string "abc") "ab"))))
  (testing "regular expression parsers"
    (is (= ((regexp* :identifier "[a-zA-Z][a-zA-Z0-9]+") "a1")
           [:identifier "a1"]))
    (is (symstr? ((regexp "[1-9][0-9]*") "123") "123"))
    (is (ip/failure? ((regexp "[1-9][0-9]*") "12a"))))
  (testing "epsilon parser"
    (is (= ((epsilon) "") [:epsilon]))
    (is (ip/failure? ((epsilon) " ")))))

;; some simple parsers for testing purposes
(def identifier (regexp* :identifier "[a-zA-Z][a-zA-Z0-9]+"))
(def number (regexp* :number "[1-9][0-9]*")) 
(def comma  (string* :comma ","))

(deftest combinator-function-test
  (testing "combinator function alt"
    (is (symstr? ((alt identifier number comma) "123") "123"))
    (is (symstr? ((alt identifier number comma) "a123") "a123"))
    (is (symstr? ((alt identifier number comma) ",") ","))
    (is (= ((alt* :inc identifier number comma) ",") [:inc ","]))
    (is (ip/failure? ((alt identifier number comma) " "))))
  (testing "combinator function cat"
    (is (symstr? ((cat number identifier) "123a123") "123" "a123"))
    (is (ip/failure? ((cat number identifier) "a123a123")))
    (is (= ((cat* :icn identifier comma number) "a123,123")
           [:icn "a123" "," "123"])))
  (testing "combinator function opt"
    (is (symstr? ((opt identifier) "a123") "a123"))
    (is (symstr? ((opt identifier) "")))
    (is (ip/failure? ((opt identifier) "123")))
    (is (= ((opt* :iopt identifier) "a123") [:iopt "a123"])))
  (testing "combinator function star"
    (is (symstr? ((star comma) ",") ","))
    (is (symstr? ((star comma) ",,,") "," "," ","))
    (is (symstr? ((star comma) "")))
    (is (= ((star* :iopt identifier) "a123") [:iopt "a123"])))
  (testing "combinator function plus"
    (is (symstr? ((plus comma) ",") ","))
    (is (symstr? ((plus comma) ",,,") "," "," ","))
    (is (ip/failure? ((plus comma) "")))
    (is (= ((plus* :iopt identifier) "a123") [:iopt "a123"])))
  (testing "some combinations"
    (is (= ((cat* :icns identifier (star (cat comma number))) "a123,345,678")
           [:icns "a123" "," "345" "," "678"]))
    (is (= ((cat* :icns identifier (star (cat comma number))) "a123")
           [:icns "a123"]))
    (is (= ((cat* :icn identifier (plus (cat comma number))) "a123,123,567")
           [:icn "a123" "," "123" "," "567"]))
    (is (ip/failure? ((cat* :icn identifier (plus (cat comma number))) "a123")))))


;; comparing parse trees of instaparse and instacomb
(def as-and-bs
  (ip/parser
   "S = AB*
     AB = A B
     A = 'a'+
     B = 'b'+"))
(def B (plus* :B (string "b")))
(def A (plus* :A (string "a")))
(def AB (cat* :AB (nt A) (nt B)))
(def S (star* :S (nt AB)))

(def wp (regexp* :whitespace "\\s+"))
(def p (cat* :s (string "a") (opt (string "b"))))
(p "ab")
(def p-white (auto-whitespace p wp))
(p-white "  a b ")

(deftest comparing-parse-trees
  (testing "comparing parse trees"
    (is (= (rest ((cat  (string "a") (string "b")) "ab"))
           (rest ((ip/parser {:s (ic/cat (ic/string "a") (ic/string "b"))}
                             :start :s) "ab"))))
    (is (= (rest ((opt  (cat (string "a") (regexp "[1-9][0-9]*"))) "a1"))
           (rest ((ip/parser {:o (ic/opt (ic/cat
                                          (ic/string "a")
                                          (ic/regexp "[1-9][0-9]*")))}
                             :start :o) "a1"))))
    (is (= (as-and-bs "aabab") (S "aabab")))
    (is (= (p "ab") (p-white "  a b ")))
    (is (ip/failure? (p "  a b ")))))

;; recursive grammars
(deftest recursive-grammars
  (testing "macro parser-comb"
    (is (= ((parser-comb A [A (alt (string "a") B) B (alt (string "b") A)]) "a")
           [:A "a"]))
    (is (= ((parser-comb A [A (alt (string "a") B) B (alt (string "b") A)]) "b")
           [:A [:B "b"]]))
    (is (ip/failure?
         ((parser-comb A [A (alt (string "a") B) B (alt (string "b") A)]) "c")))
    (is (= ((parser-comb  term [term (alt (cat prod (string "+") term) prod)
                                prod (alt (cat prim (string "*") prod) prim)
                                prim (alt (cat (string "(") term (string ")")) number)
                                number (regexp "[1-9][0-9]*")])
            "(1+2)*(1+3)")
           [:term
            [:prod
             [:prim
              "("
              [:term [:prod [:prim [:number "1"]]]
               "+" [:term [:prod [:prim [:number "2"]]]]]
              ")"]
             "*"
             [:prod
              [:prim
               "("
               [:term [:prod [:prim [:number "1"]]]
                "+" [:term [:prod [:prim [:number "3"]]]]]
               ")"]]]]))))

;; hiding content
(def seq-of-A-or-B (star* :parens-wrapped (alt (string "a") (string "b"))))
(def paren-wrapped
  (cat* :parens-hidden (hide (string "(")) seq-of-A-or-B (hide (string ")"))))
(def nt-hide-tag
  (cat* :parens-hidden (hide (string "("))
        (hide-tag (nt seq-of-A-or-B)) (hide (string ")"))))

(deftest hiding-parens
  (testing "hiding-parens"
    (is (= (paren-wrapped "(aba)")
           [:parens-hidden "a" "b" "a"]))
    (is (= (nt-hide-tag "(aba)")
           [:parens-hidden "a" "b" "a"]))
    (is (= ((hide-tag paren-wrapped) "(aba)")
           '("a" "b" "a")))))



          
        
