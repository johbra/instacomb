Im Zusammenhang mit der Entwicklung von domänenspezifischen Sprachen
(DSLs) haben Techniken für die Entwicklung von Parsern steigende
Bedeutung erfahren. Zu diesen Techniken gehören die Parserkombinatoren,
für die es Implementierungen für verschiedene Programmiersprachen gibt.
Sie sind dadurch gekennzeichnet, dass sie und deren Resultate in die
Wirtssprache eingebettet bzw. weiterverarbeitet werden können. Für die
Programmiersprache Clojure steht mit *Instaparse* ein leistungsfähiger
Parser zur Verfügung, der die vollständige Syntax der zu definierenden
Sprache (z.B. der DSL) in einer der Varianten der Backus-Naur-Form als
Zeichenkette erwartet. In diesem Aufsatz wird geschildert, wie
Instaparse als Implementierungsbasis für eine
Parserkombinator-Bibliothek genutzt werden kann.

Einleitung
==========

Es gibt viele Wege einem Rechner zu sagen, was er tun soll. Ein
Ingenieur schreibt möglicherweise ein MATLAB-Programm, ein
Datenbankadministrator benutzt SQL für seine Aufgaben, ein Entwickler
von integrierten Schaltungen beschreibt diese grafisch in Form von
klassischen Schaltplänen oder – z.B. zum Zwecke ihre Simulation – mit
einer text-basierten Hardware-Beschreibungssprache und ein Buchhalter
löst seine Probleme mit Formeln innerhalb einer Tabellenkalkulation.
Jeder benutzt eine „Sprache“, die auf seinen Problembereich
zugeschnitten ist, eine domänenspezifische Sprache (DSL[^1]). Eine DSL
besitzt eine auf den jeweiligen Einsatzzweck zugeschnittene Syntax und
Semantik. Darin unterscheiden sich DSLs von Programmiersprachen, die als
Universalsprachen konzipiert sind.

Am Beispiel der elektronischen Schaltung können zwei grundlegende
Varianten von DSLs unterschieden werden: solche, die in Textform, der
Syntax einer Hardware-Beschreibungssprache folgend, aufgeschrieben
werden und solche, die grafische Mittel verwenden, wie eben die
erwähnten Schaltpläne. Im weiteren Verlauf werden hier DSLs aus dem
Anwendungsbereich der Software-Entwicklung betrachtet. Aber auch hier
kommen sowohl text- als auch grafik-basierte Sprache zum Einsatz. So
ließe sich ein endlicher Automat sowohl durch einen strukturierten Text
als auch durch ein Zustandsübergangsdiagramm beschreiben. Es werden im
Folgenden aber nur textbasierte DSLs behandelt.

Eine weitere gängige Klassifikation von text-basierten DSLs
unterscheidet zwischen internen und externen DSLs. Diese Begriffe
beziehen sich auf das Verhältniszur Wirtssprache[^2], in der die in der
DSL beschriebenen Komponenten verwendet bzw. in die sie eingebettet
werden. Bei internen DSLs stellt die Wirtssprache die Mittel zur
Definition einer DSL und ihrer Verwendung bereit. Bezüglich der
syntaktischen Gestaltung ist man bei einer internen DSL in der Regel an
die syntaktischen Grundschemata der Wirtssprache gebunden. Ein wichtiger
Vorteil besteht in der Möglichkeit der Nutzung ihrer Infrastruktur, also
insbesondere die Werkzeuge der Entwicklungsumgebung.

Eine externe DSL hingegen erlaubt, die Syntax vollkommen unabhängig von
der der Wirtssprache zu definieren. Allerdings bietet die
Entwicklungsumgebung in der Regel keine Unterstützung beim Schreiben von
Texten in der DSL und insbesondere das Parsen von DSL-Texten und die
Code-Generierung müssen für eine externe DSL jeweils neu entwickelt
werden. Lukas Renggli@RE10 zeigt allerdings am Beispiel der Wirtssprache
Smalltalk in der Entwicklungsumgebung Pharo[^3], wie auch für externe
DSLs eine nahtlose Benutzung der Werkzeuge der Entwicklungsumgebung
ermöglicht werden kann. Eine Anwendung dieser Technik zur Entwicklung
einer DSL für die Spezifikation klassenloser Objekte wird in
@BraCraKra2012 gezeigt.

Tiefergehende Klassifikationen von DSLs sind u.a. bei Martin
Fowler@Fow10 und Debasish Gosch@Gh11 zu finden.

In den Sprachen der Lisp-Familie, der auch die Sprache Clojure[^4]
zuzurechnen ist, ist die Verwendung von internen DSLs in Form von
Lisp-Macros eine seit Jahrzehnten geübte Praxis. Macro-Definitionen und
Macro-Aufrufe werden in Form von symbolischen Ausdrücken aufgeschrieben,
folgen also der grundlegenden Lisp-Syntax. Selbstverständlich gibt es
aber auch für Lisp-Sprachen Funktionsbibliotheken für die
Implementierung von externen DSLs. Eine davon ist der von Mark Engelbert
entwickelte Parser-Generator *Instaparse* @Engelberg14 für Clojure.

In Abschnitt [sec:instaparse] wird zunächst ein Überblick über die
Funktionsweise von Instaparse als Parsergenerator gegeben, bevor dann im
Abschritt [sec:parserkombinatoren] die Technik der Parserkombinatoren
erläutert wird. Schließlich wird dann im Abschnitt
[sec:funktionsbibliothek] die Entwicklung einer Funktionsbibliothek
gezeigt, die einen Parserkombinator auf der Grundlage von Instaparse
bildet.

Instaparse - Überblick {#sec:instaparse}
======================

Das Kernstück von Instaparse stellt die Clojure-Funktion `parser` dar,
die im einfachsten Fall die Grammatik einer Sprache in einer Variante
der erweiterten Backus-Naur-Form[^5] (EBNF @ISOIEC14977) als
String-Argument (Abschnitt [sec:gramm-als-zeich]) erwartet und einen
Parser für diese Sprache als Resultat liefert. Eine zweite Möglichkeit
besteht in der Definition der Grammatik als so genannte Grammar-Map.
Diese Variante wird in Abschnitt [sec:gramm-als-gramm] behandelt.

Grammatikdefinition als Zeichenkette {#sec:gramm-als-zeich}
------------------------------------

Als einfaches Beispiel der Anwendung der Funktion `parser` soll die
folgende Grammatik[^6] dienen:

         S = AB*
         AB = A B
         A = 'a'+
         B = 'b'+

Diese Grammatik akzeptiert beliebige Folgen der Buchstaben „a“ und „b“.
Durch den folgenden Clojure-Ausdruck wird die Funktion
`insta/parser`[^7] mit dieser Grammatik als Argument aufgerufen und der
resultierende Parser an das Symbol `as-and-bs` gebunden:

    (def as-and-bs
      (insta/parser
        "S = AB*
         AB = A B
         A = 'a'+
         B = 'b'+"))

Der Aufruf des Parsers mit

     (as-and-bs "aaaaabbbaaaabb")

liefert dann einen Parsebaum in Form eines geschachtelten Vektors[^8]:

    [:S
     [:AB [:A "a" "a" "a" "a" "a"] [:B "b" "b" "b"]]
     [:AB [:A "a" "a" "a" "a"] [:B "b" "b"]]]

Jeder Vektor enthält dabei das jeweilige nicht-terminale Symbol in Form
eines Clojure-Keywords[^9] gefolgt entweder von weiteren Vektoren
(Teilbäumen) oder den terminalen Symbolen (hier die Buchstaben „a“ und
„b“).

Ein anderes, etwas anspruchsvolleres Beispiel aus @Engelberg14 definiert
eine Grammatik, die mit Vorausschau (lookahead) auf Eingaben arbeitet
und Zeichenketten akzeptiert, die aus einer „a“-Folge, gefolgt von einer
gleichlangen „b“-Folge, gefolgt von einer gleichlangen „c“-Folge
bestehen.

    (def abc
      (insta/parser
        "S = &(A 'c') 'a'+ B
         A = 'a' A? 'b'
         <B> = 'b' B? 'c' "))

Das &-Zeichen kennzeichnet den Lookahead-Parser. Das Fragezeichen
kennzeichnet das davor stehende Symbol als optional.

Wird der Parser `abc` mit

    (abc "aaabbbccc")  

aufgerufen, liefert er den folgenden Vektor:

    [:S "a" "a" "a" "b" "b" "b" "c" "c" "c"]

Dass hier kein geschachtelter Vektor ausgegeben wird, liegt daran, dass
in der Grammatik das nicht-terminale Symbol B in spitze Klammern gesetzt
ist. Auf diese Weise erlaubt Instaparse die Vereinfachung des
Parsebaums.

Eine weitere, sehr praktische Funktionaliät von Instaparse ermöglicht,
einen Parser zu erzeugen, der automatisch Leerraum (Leezeichen,
Zeilenwechsel etc.) zwischen nicht-terminalen Symbolen zulässt.

Dazu definiert man zunächst einen Parser, der Leerraum erkennt, z.B. auf
diese Weise:

    (def whitespace
      (insta/parser
        "whitespace = #'\\s+'"))

Hier wird übrigens davon Gebrauch gemacht, dass in einer Grammatik auch
reguläre Ausdrücke verwendet werden dürfen.

Ergänzt man nun in der Definition des `abc`-Parser den Aufruf von
insta/parser um das Keyword-Argument ` :auto-whitespace` mit dem soeben
definierten `whitespace`-Parser

    (def abc
      (insta/parser
        "S = &(A 'c') 'a'+ B
         A = 'a' A? 'b'
         <B> = 'b' B? 'c' "
         :auto-whitespace whitespace))

so lässt der `abc`-Parser nun auch Leerzeichen zwischen den Buchstaben
zu. Def Aufruf

    (abc "a a a bbb c c c")  

liefert wieder den Vektor `[:S "a" "a" "a" "b" "b" "b" "c" "c" "c"]`.

Grammatikdefinition als Grammar-Map {#sec:gramm-als-gramm}
-----------------------------------

Neben der Definition der Grammatik durch eine EBNF-Zeichenkette erlaubt
Instaparse alternativ die Angabe einer Grammar-Map. Hierbei handelt es
sich zunächst um eine Standard-Datenstruktur von Clojure, der Map. Eine
Map ist eine Kollektion von Schlüssel-Wert-Paaren. Als Schlüssel werden
sehr häufig Clojure-Keywords verwendet. So auch bei den Grammar-Maps:
Nicht-terminale Symbole werden zu Keywords. Die zugehörigen Regeln
werden in Form von funktionalen Ausdrücken formuliert, wobei die
Funktionen den Konstrukten der EBNF entsprechen. So wird z.B. aus „A?“
`(opt A)`, aus „&A“ `(look A)` oder aus „A+“ `(plus A)`. Diese
Funktionen werden in @Engelberg14 in Anlehnung an die
Parserkombinatoren[^10] als „Combinators” bezeichnet. Eine vollständige
Liste der *Combinators* ist in @Engelberg14 zu finden.

Die im Abschnitt [sec:gramm-als-zeich] als Beispiel benutzte Grammatik

    S = &(A 'c') 'a'+ B
    A = 'a' A? 'b'
    <B> = 'b' B? 'c'

wäre als Grammar-Map wie folgt aufzuschreiben:

    (def abc-grammar-map
      {:S (cat (look (cat (nt :A) (string "c")))
               (plus (string "a"))
               (nt :B))
       :A (cat (string "a") (opt (nt :A)) (string "b"))
       :B (hide-tag (cat (string "b") (opt (nt :B)) 
                                 (string "c")))})

Der Combinator `cat` dient der Konkatenation von Syntaxelementen, was in
der String-Form einfach durch hintereinander Schreiben ausgedrückt wird.
Terminale Zeichenketten müssen mit dem Combinator `string` versehen
werden. Der Combinator `hide-tag` entspricht den spitzen Klammern in der
String-Form der Grammatik. Mit der `nt`-Funktion wird auf
nicht-terminale Symbole Bezug genommen.

Der Ausdruck

    (insta/parser abc-grammar-map :start :S)

erzeugt einen Parser, der mit dem „abc“-Parser aus
Abschnitt [sec:gramm-als-zeich] identisch ist und wieder
Buchstabenfolgen akzeptiert, die aus gleich langen Folgen der Buchstaben
„a“ „b“ und „c“ bestehen. Wird die `parser`-Funktion mit einer
Grammar-Map als Argument aufgerufen, muss das Keyword-Argument `:start`
mit der Startregel der Grammatik angegeben werden.

Mark Engelberg weist in @Engelberg14 darauf hin, dass mit den
Grammar-Maps keine erweiterten Möglichkeiten bei der Konstruktion von
Parsern mithilfe von Instaparse einher gehen. Darüber hinaus stellt er
fest, dass es der Grammar-Map – verglichen mit der String-Form – auch an
der Prägnanz der Ausdrucksweise bei der Formulierung einer Grammatik
mangelt. Dem ist sicherlich zuzustimmen. Parserkombinatoren, wie sie im
Abschnitt [sec:parserkombinatoren] behandelt werden, besitzen einen
anderen Charakter: Sie bestehen aus einer Menge von Funktionen höherer
Ordnung, die Parser als Resultate liefern, die funktional zu komplexeren
Parsern zusammengefügt werden können. Dennoch haben die Grammar-Maps
ihre Berechtigung insbesondere dann, wenn die zu definierende Grammatik
gar nicht als EBNF-String vorliegt sondern aus Eingabedaten erst erzeugt
werden müsste.

Auch wenn Instaparse selbst keine Parserkombinator-Bibliothek im engeren
Sinne zur Verfügung stellt, bietet es – wie in
Abschnitt [sec:funktionsbibliothek] gezeigt wird – eine gute Grundlage
für die Schaffung einer solchen.

Implementierung von DSL-Parsern {#sec:parserkombinatoren}
===============================

Im Zusammenhang mit der Entwicklung von domänenspezifischen Sprachen
werden verschiedene Implementierungsvarianten diskutiert. Hierzu zählen
in erster Linie die Abschnitt [sec:parsergeneratoren] kurz vorgestellten
Parsergeneratoren. Die zweite Technik, auf die hier besonderers
Augenmerk gelegt wird, sind die Parserkombinatoren, die in
Abschnitt [sec:parsergeneratoren] vorgestellt werden. Eine
Gegenüberstellung dieser beiden Techniken wird in @van2009comparing
vorgenommen.

Parsergeneratoren {#sec:parsergeneratoren}
-----------------

Bei dieser Implementierungsvariante verarbeitet der Generator eine in
der Regel in EBNF-Syntax vorliegende Grammatik und erzeugt daraus
Parser-Code für verschiedene Zielsprachen. Die Ausführung des
Parser-Codes zum Zwecke der Analyse einer konkreten Eingabe führt dann
z.B. zur Erzeugung eines Parsebaums, der in einem speziellen
Zwischencode vorliegt, und dann von einem anderen Programm, das in der
Zielsprache implementiert ist, verarbeitet werden muss. Typische
Vertreter dieser Vorgehensweise sind *ANTLR*[^11] und *JavaCC*[^12].

Eingebettet in die Entwicklungsumgebung *Eclipse*[^13] ist der
Parsergenerator *Xtext*[^14]. Kennzeichnend für Xtext ist nicht nur die
nahtlose Unterstützung durch die Eclipse-typischen Werkzeuge sondern
auch die Erzeugung eines Java-Klassenmodells für den abstrakten
Parsebaum. Das erleichtert die weitere Verarbeitung der Parserausgabe.

Auch Instaparse ist ein Beispiel für einen Parsergenerator.
Kennzeichnend für Instaparse ist, dass der Parsebaum als
Clojure-Datenstruktur erzeugt wird, die dann von anderen
Clojure-Funktionen problemlos weiter verarbeitet werden kann. So ist
Instaparse – änlich wie Xtext in Java – vollständig in die Wirtssprache
integriert.

Parserkombinatoren {#sec:parserkombinatoren}
------------------

Eine besondere Bedeutung für die Implementierung von DSLs hat die
Nutzung von Parserkombinator-Bibliotheken erlangt. Eine gängige
Definition besagt, dass ein Parserkombinator eine Funktion ist, die
einen oder mehrere Parser als Argument akzeptiert und daraus einen neuen
Parser erzeugt.

Die Technik der Parserkombinatoren ist seit mehr als zwei Jahrzehnten
bekannt. Hutton @JFP:1457488 beschreibt die Ursprünge der Technik und
Frost u.a. @Frost:2007:MET:1621410.1621425 beschreiben, wie auch
fortschrittliche Parsetechniken mit Parserkombinatoren effizient
implementiert werden können.

Eine der bedeutendsten Parserkombinator-Bibliotheken ist *Parsec*[^15],
die für die funktionale Programmiersprache Haskell entwickelt wurde.
Viele weitere Implementierungen beziehen sich auf die in Parsec
verwirklichten Ideen. Parsec wurde inzwischen auch in andere
Programmiersprachen übertragen, so z.B. in Java (*JParsec*), Ruby (*Ruby
Parsec*) und F\# (*FParsec*).

Es gibt auch eigenständige Entwicklungen von
Parserkombinator-Bibliotheken für andere Programmiersprachen. Hierzu
zählen z.B. *PetitParser* @RenGirDucNie2010, der für Pharo-Smalltalk
entwickelt wurde inzwischen aber auch für Java[^16] zur Verfügung steht.
Ein Anwendungsszenario für PetitParser findet sich z.B. in @KBC11. Auch
für Scala gibt es mit *Parsers*[^17] eine entsprechende Bibliothek.

Als wichtige Vorteile von Parserkombinatoren gelten:

-   ihre vollständige Integration in die Wirtssprache

-   Verknüpfung von Parsen und Verarbeitung

-   ihr modularer Aufbau

-   die Unterstützung der testgetriebenen Entwicklung

Auch für Clojure gibt es bereits Parserkombinatoren. Zu ihnen gehören
z.B. *The Parsatron*[^18], das sich eng an das Vorbild Parsec anlehnt
oder *zetta-parser*[^19]. Diese Bibliotheken stellen eine Reihe von
primitiven Parsern bereit, die mit den Kombinator-Funktionen zu
komplexeren Parsern zusammengefügt werden können. Dazu steht eine
Funktion oder ein Macro, meist `defparser` genannt, zur Verfügung, mit
dem eine neue Parserfunktion erzeugt werden kann. Eine weitere Funktion,
meist `run` genannt, akzeptiert dann einen so erzeugten Parser und den
Eingabestrom als Argumente und liefert das Parseresultat.

Die Abschnitt [sec:funktionsbibliothek] vorgestellten
Parserkombinator-Funktionen auf der Basis von Instaparse arbeiten etwas
anders. Sie können als Funktion direkt auf den Eingabestrom angewendet
werden, eine `run`-Funktion erübrigt sich.

Funktionsbibliothek für Parserkombinatoren auf Basis von Instaparse {#sec:funktionsbibliothek}
===================================================================

In den folgenden Abschnitten werden die Benutzung und die
Implementierung eines Satzes von Parserkombinator-Funktionen auf der
Basis von Instaparse gezeigt, der im Folgenden mit *InstaComb*
bezeichnet wird.

Einführendes Beispiel {#sec:einf-beisp}
---------------------

Die Verwendung der InstaComb-Funktionen ähnelt sehr stark den
Funktionen, die innerhalb einer Grammar-Map, wie in
Abschnitt [sec:gramm-als-gramm] gezeigt, benutzt werden können. Als
Beispiel wird zunächst die in Abschnitt [sec:gramm-als-zeich] gezeigte
Grammatik

    (def as-and-bs
      (insta/parser
        "S = AB*
         AB = A B
         A = 'a'+
         B = 'b'+"))

benutzt. Dieser Parser könnte jetzt wie folgt mit InstaComb-Funktionen
geschrieben werden:

    (def B (plus (string "b")))
    (def A (plus (string "a")))
    (def AB (cat A B))
    (def S (star AB))

Das Symbol `S` ist nun an einen Parser gebunden, der direkt auf eine
Zeichenkette angewendet werden kann:

    (S "aabbbab")  ;; => [:G__4065 "a" "a" "b" "a" "b"]

Vergleicht man das Parseergebnis mit dem der oben definierten,
originalen Instaparse-Variante

    (as-and-bs "aabab")
    ;; => [:S [:AB [:A "a" "a"] [:B "b"]] 
    ;;        [:AB [:A "a"] [:B "b"]]]

so erkennt man zwei Unterschiede:

1.  Der InstaComb-Parser erzeugt einen flachen Vektor.

2.  Da im InstaComb-Parser keine Namen für nicht-terminale Symbole
    vergeben wurden, werden intern generierte Symbole verwendet (hier:
    `:G__4065`)

Zum ersten Punkt ist anzumerken, dass in InstaComb Teilregeln nur dann
als Teilbäume im Parseresultat auftauchen, wenn sie mit der Funktion
`nt` versehen werden. Um also die gleiche Baumstruktur wie bei der
Instaparse-Variante zu erzeugen, schreibt man die Regeln wie folgt:

`   (def B (plus (string "b")))`\
`   (def A (plus (string "a")))`\
`   (def AB (cat (`**`nt`**` A) (`**`nt`**` B)))`\
`   (def S (star (`**`nt`**` AB)))`

Der Aufruf von `S` wie oben liefert nun:

    [:G__4086
     [:G__4084 [:G__4081 "a" "a"] [:G__4079 "b"]]
     [:G__4084 [:G__4081 "a"] [:G__4079 "b"]]]

Möchte man die automatisch generierten Symbole durch eigene Namen
ersetzen, kann man die „`*`-Varianten“ der InstaComb-Funktionen
benutzen, die als weiteres Argument einen Regelnamen erlauben:

    (def B (plus* :B (string "b")))
    (def A (plus* :A (string "a")))
    (def AB (cat* :AB (nt A) (nt B)))
    (def S (star* :S (nt AB)))

Damit ergibt sich schließlich::

    (S "aabab")
    ;; => [:S [:AB [:A "a" "a"] [:B "b"]] 
    ;;        [:AB [:A "a"] [:B "b"]]]

Beispiel: Arithmetische Ausdrücke {#sec:beisp-arithm-ausdr}
---------------------------------

In @Pharo2013 wird in Kapitel 18 (PetitParser: Building Modular Parsers)
ein Parser für einfache arithmetische Ausdrücke als Beispiel für die
Anwendung von *PetitParser* herangezogen. Abbildung [fig:syntaxdiagramm]
zeigt das zugehörige Syntaxdiagramm.

![image](    syntaxdiagramm.png)

Die Grammatik beschreibt also Ausdrücke der Art $(1+2)*(3+4)$.

Die Implementierung in PetitParser sieht wie folgt aus:

    term := PPDelegateParser new. 
    prod := PPDelegateParser new. 
    prim := PPDelegateParser new.
    term setParser: (prod , $+ asParser , term) / prod.
    prod setParser: (prim , $* asParser , prod ) / prim.
    prim setParser: ($( asParser , term , $) asParser 
                    / number.

Mit der Nachricht `asParser` wird ein Parser für das die Nachricht
empfangende Zeichen erzeugt. Das Komma dient der Konkatenation zweier
Parser, der Schägstrich steht für die Alternative (der senkrechte Strich
in der EBNF). Nicht-terminale Symbole werden durch Variablen (hier:
`term`, `prod` und `prim`) repräsentiert. Ein Parser für Zahlen
(`number`) wird hier als anderweitig definiert angenommen.

Wichtig ist an dieser Stelle anzumerken, dass den drei Variablen `term`,
`prod` und `prim` zunächst Platzhalter-Parser (Exemplare der Klasse
`PPDelegateParser`) zugewiesen werden. D.h. die Variablen verweisen auf
Objekte, die später ersetzt werden. Das ist notwendig, weil die
Syntaxregeln wechselseitig rekursiv sind. Mithilfe der Nachricht
`setParser:` mutieren die Parser dann zu denjeniigen, die den an
`setParser:` als Argument übergebenen Syntaxregeln entsprechen.

Für die Technik, Variablen zuerst an Platzhalter zu binden, die dann
später mutieren, gibt es im funktionalen Paradigma keine Entsprechung.
Soll die Definition von Grammatiken mit rekursiven Regeln in InstaComb
ermöglicht werden, ist eine andere Lösung erforderlich. In Clojure
können dafür Macro-Aufrufe verwendet werden, in die die
Kombinatoraufrufe eingebettet werden. In InstaComb steht dafür das Macro
`parser-comb` zur Verfügung:

    (def expr
      (parser-comb
       term
       [term (alt (cat prod (string "+") term) prod)
        prod (alt (cat prim (string "*") prod) prim)
        prim (alt (cat (string "(") term (string ")"))
                  number)
        number (regexp "[1-9][0-9]*")]))

Das Macro erwartet als erstes Argument den Namen der Startregel, hier
`term`. Als zweites Argument ist ein Vektor anzugeben, der aus Paaren
besteht, dessen erstes Element jeweils der Name eines nicht-terminalen
Symbol und dessen zweites Element eine InstaComb-Ausdruck ist. So werden
oben die drei Regeln für `term`, `prod` und `prim` definiert. Die
`number`-Regel ist hier ein regulären Ausdruck.

Dadurch, dass die Argumente eines Macro-Aufrufs unausgewertet an das
Macro übergeben werden, entsteht kein Problem mit der Rekursivität der
Regeln. Die Implementierung des Macros wird in
Abschnitt [sec:macro-parser-comb] erläutert.

Das Macro erzeugt eine Kombinatorfunktion, die wie jede andere
angewendet wird. Der Ausdruck

    (expr "(1+2)*(3+4)")

liefert wieder einen Parsebaum in Form eines geschachtelten Vektors:

    [:term
     [:prod
      [:prim
       "("
       [:term
        [:prod [:prim [:number "1"]]]
        "+"
        [:term [:prod [:prim [:number "2"]]]]]
       ")"]
      "*"
      [:prod
       [:prim
        "("
        [:term
         [:prod [:prim [:number "3"]]]
         "+"
         [:term [:prod [:prim [:number "4"]]]]]
        ")"]]]]

Implementierung {#sec:implementierung}
---------------

Betrachten wir noch einmal den einfachen Parser aus
Abschnitt [sec:gramm-als-zeich]:

      (insta/parser
       "S = AB*
         AB = A B
         A = 'a'+
         B = 'b'+"))

Wertet man diesen Ausdruck aus, zeigt Instaparse das Resultat (den
Parser) in Form einer Clojure-Map:

    {:grammar
     {:S
      {:red {:reduction-type :hiccup, :key :S},
       :tag :star,
       :parser {:tag :nt, :keyword :AB}},
      :AB
      {:red {:reduction-type :hiccup, :key :AB},
       :tag :cat,
       :parsers ({:tag :nt, :keyword :A}
                 {:tag :nt, :keyword :B})},
      :A
      {:red {:reduction-type :hiccup, :key :A},
       :tag :plus,
       :parser {:tag :string, :string "a"}},
      :B
      {:red {:reduction-type :hiccup, :key :B},
       :tag :plus,
       :parser {:tag :string, :string "b"}}},
     :start-production :S,
     :output-format :hiccup}

Dabei ist der Schlüssel `:grammar` mit der internen Repräsentation der
Grammatik assoziiert. Diese lässt sich durch Anwenden der Funktion
`:grammar` extrahieren. Der Ausdruck

    (:grammar
     (insta/parser
      "S = AB*
         AB = A B
         A = 'a'+
         B = 'b'+"))

liefert:

    {:S
     {:red {:reduction-type :hiccup, :key :S},
      :tag :star,
      :parser {:tag :nt, :keyword :AB}},
     :AB
     {:red {:reduction-type :hiccup, :key :AB},
      :tag :cat,
      :parsers ({:tag :nt, :keyword :A} 
                {:tag :nt, :keyword :B})},
     :A
     {:red {:reduction-type :hiccup, :key :A},
      :tag :plus,
      :parser {:tag :string, :string "a"}},
     :B
     {:red {:reduction-type :hiccup, :key :B},
      :tag :plus,
      :parser {:tag :string, :string "b"}}}

Um nun z.B. zwei Instaparse-Parser für die Definition einer
Kombinatorfunktion zu verknüpfen, können die Grammatiken der beiden
Parser mit der `:grammar`-Funktion extrahiert und gemäß der Aufgabe der
Kombinatorfunktion geeignet zusammengefügt werden. Auf dieser Grundlage
beruht die Implementierung der InstaComb-Funktionen.

Ein einfaches Beispiel für diese Vorgehensweise ist die Definition der
in Abschnitt [sec:gramm-als-zeich] gezeigten
auto-whitespace-Funktionalität von Instaparse als Funktion für
InstaComb:

    (defn auto-whitespace [p whitespace-parser]
      (insta/parser (:grammar p)
                    :start (:start-production p)
                    :auto-whitespace whitespace-parser))

Die Funktion erwartet einen Parser `p` und einen `whitespace-parser` als
Argumente. Sie ruft die Funktion `parser` von Instaparse mit der
Grammatik von `p`, der Startproduktion von `p` und dem
`whitespace-parser` auf.

### Primitive Parser {#sec:primitve-parser}

Die Überführung des Instaparse-Parsers für Zeichenketten in die
InstaComb-Funktion `string` erfolgt einfach durch Erzeugung einer
Instaparse-Grammar-Map unter Verwendung der Funktion `c/string`[^20]:

    (defn string
      "erzeugt einen String-Parser für str"
      [str]
      (let [rule-name (keyword (gensym))]
        (insta/parser {rule-name  (c/string str)}
                      :start rule-name)))

An die lokale Variable `rule-name` wird ein automatisch generiertes
Symbol gebunden, das dann in der Grammar-Map als Regelname (und
Startregel) verwendet wird. So liefert dann z.B. der Ausdruck

    ((string "abc") "abc")  ;;=> [:G__3975 "abc"]

das Parseresultat mit dem Regelnamen `:G__3975`.

Wie schon in Abschnitt [sec:einf-beisp] erwähnt, gibt es für die
InstaComb-Funktionen immer auch eine `*`-Variante, so auch

    (defn string*
      "erzeugt einen String-Parser für str"
      [rule-name str]
      (insta/parser {rule-name  (c/string str)} 
                    :start rule-name))

Hier wird der Regelname als erstes Argument erwartet.

Zu den primitiven Parsern zählen wir auch die für reguläre Ausdrücke.
Die Funktion

    (defn regexp
      "erzeugt einen Parser für den regulären Ausdruck re"
      [re]
      (let [rule-name (keyword (gensym))] 
        (insta/parser {rule-name  (c/regexp re)} 
                      :start rule-name)))

ist im Grunde genau so aufgebaut wie die Funktion `string`. Sie erwartet
einen regulären Ausdruck als Zeichenkette und baut dann einen Parser
mithilfe der Grammar-Map-Funktion `c/regexp`

Hier und im Folgenden wird auf die Angabe der `*`-Varianten der
Funktionen verzichtet.

### Erzeugung von Kombinatorfunktionen {#sec:erzeugung-kombinatorfunktionen}

Für die Erzeugung nicht-primitiver Parser unterscheiden wir zwischen
einstelligen Kombinatoren (z.B. die Operatoren „?” und „+“ aus der EBNF)
und den mehrstelligen, wie z.B. die Konkatenation und die Alternative
(Operator „$|$“ der EBNF). Die entsprechenden Kombinatorfunktionen
werden unter Verwendung zweier Hilfsfunktionen (`combinator-function`
für die einstelligen und `combinator-function-n` für die mehrstelligen
Kombinatoren) erzeugt:

    (def alt (combinator-function-n :alt))
    (def cat (combinator-function-n :cat))
    (def opt (combinator-function :opt))
    (def star (combinator-function :star))
    (def plus (combinator-function :plus))
    (def look (combinator-function :look))

Für die weitere Erläuterung der Implementierung beschränken wir uns hier
auf die unären Kombinatoren.

Die Funktion `combinator-function`

    (defn combinator-function
      "akzeptiert ein instaparse combinator keyword eines 
       unären combinators und erzeugt eine Funktion, die 
       einen Parser p akzeptiert und einen Parser für den 
       combinator erzeugt"
      [combinator-keyword]
      (fn 
        [p]
        (let [rule-name (keyword (gensym))] 
          (insta/parser
           (assoc
               (if (terminal? p) {} (:grammar p))
             rule-name
             {:tag combinator-keyword,
              :parser (t-or-nt p)})
           :start rule-name))))

erwartet als Argument ein Schlüsselwort aus der Menge der von Instaparse
für die Kennzeichnung der Kombinatoren verwendeten:

    :alt :cat :opt :star :plus :look

Die Funktion liefert als Rückgabewert wiederum eine Funktion, die

-   einen Parser (`p`) als Argument erwartet,

-   einen lokalen Regelnamen (`rule-name`) erzeugt und dann

-   die `parse`-Funktion von Instaparse aufruft mit

    -   einer unter Verwendung von `p` neu erzeugten Grammatik und

    -   `rule-name` als Startregel.

Das Erzeugen der neuen (kombinierten) Grammatik geschieht mithilfe des
Ausdrucks

           (assoc
               (if (terminal? p) {} (:grammar p))
             rule-name
             {:tag combinator-keyword,
              :parser (t-or-nt p)})

Mithilfe von `assoc` kann einer Clojure-Map (erstes Argument) ein
weiteres Schlüssel-Wert-Paar (zweites und drittes Argument) hinzugefügt
werden. In Abhängigkeit von der Prüfung, ob der Parser `p` ein
terminales Symbol erkennt[^21] wird entweder eine leere Map oder die
Grammatik von `p` an `assoc` übergeben.

Das zweite Argument von `assoc` ist der neue Regelname `rule-name`. Das
dritte Argument ist seinerseits eine Map, die den Schlüssel `:tag` für
das an die Funktion `combinator-function` übergebene Schlüsselwort
enthält und für den Schlüssel `:parser` den Parser `p`. Die Funktion
`t-or-nt` sorgt dabei für die in Abschnitt [sec:einf-beisp] Eigenschaft
der InstaComb-Funktionen, dass Teilregeln nur dann als Teilbäume im
Parseresultat auftauchen, wenn sie mit der Funktion `nt` versehen
werden.

So erzeugt der Ausdruck`(opt (string "abc"))` den Parser

    {:grammar
     {:G__4185
      {:red {:reduction-type :hiccup, :key :G__4185},
       :tag :opt,
       :parser {:string "abc", :tag :string, :hide nil}}},
     :start-production :G__4185,
     :output-format :hiccup}

mit der Regel `:G__4185` für die Zeichenkette `"abc"` mit dem `:tag`
`:opt`, während der Parser für den Ausdruck `(opt (nt (string "abc")))`

    {:grammar
     {:G__4206
      {:red {:reduction-type :hiccup, :key :G__4206},
       :tag :opt,
       :parser {:hide nil, :tag :nt, :keyword :G__4204}},
      :G__4204
      {:non-terminal true,
       :red {:reduction-type :hiccup, :key :G__4204},
       :tag :string,
       :string "abc"}},
     :start-production :G__4206,
     :output-format :hiccup} 

eine zweite Regel mit dem `:tag` `:nt` enthält.

Die Funktion `combinator-function-n` für die Erzeugung der mehrstelligen
InstaComb-Kombinatoren ist ein wenig komplexer, da sie eine beliebige
Anzahl von zu kombinierenden Parsern verarbeiten muss. Das
zugrundeliegende Wirkungsprinzip entspricht aber dem der Funktion
`combinator-function`. Auf eine detaillierte Behandlung wird deshalb an
dieser Stelle verzichtet.

### Macro `parser-comb` {#sec:macro-parser-comb}

Das Macro `parser-comb` erwartet – wie schon in
Abschnitt [sec:beisp-arithm-ausdr] gezeigt wurde – als zweites Argument
einen Vektor mit Paaren aus Regelnamen und InstaComb-Ausdrücken. Das
Wirkungsprinzip des Macros besteht darin, diesen Vektor in eine
Instaparse-Grammar-Map zu verwandeln und diese dann an die Funktion
`parser` zu übergeben:

    (defmacro parser-comb 
      [sym rules]
      `(let [gr# (combinator-expression->grammarmap ~rules)]
         (insta/parser (eval gr#) :start (keyword '~sym))))

Die Umwandlung wird von dem Macro `combinator-expression->grammarmap`
vorgenommen. Zum Beispiel liefert der Aufruf

    (combinator-expression->grammarmap
     [A (alt (string "a") B) B (alt (string "b") A)])

die Grammar-Map[^22]:

    {:B
     (instaparse.combinators/alt
      (instaparse.combinators/string "b")
      (instaparse.combinators/nt :A)),
     :A
     (instaparse.combinators/alt
      (instaparse.combinators/string "a")
      (instaparse.combinators/nt :B))}

Diese dient dann, wie in Abschnitt) [sec:gramm-als-gramm] beschrieben,
als Argument für die `parser`-Funktion.

Als

### Abschließendes Beispiel {#abschließendes-beispiel .unnumbered}

kommen wirl hier noch einmal auf den Parser aus
Abschnitt [sec:gramm-als-gramm] zurück, der mithilfe von
Lookahead-Regeln in der Lage ist, eine Zeichenfolge zu erkennen, die aus
gleich langen Teilfolgen der Buchstaben "‘a"’, "‘b"’ und "‘c"’ besteht.
Die Formulierung als Instaparse-Grammar-Map lautete:

    (def abc
      (insta/parser
       {:S (c/cat (c/look (c/cat (c/nt :A) (c/string "c")))
                  (c/plus (c/string "a"))
                  (c/nt :B))
        :A (c/cat (c/string "a") (c/opt (c/nt :A))
                  (c/string "b"))
        :B (c/cat (c/string "b") (c/opt (c/nt :B))
                  (c/string "c"))}
       :start :S))

Mithilfe des InstaComb-Macros `parser-comb` kann der Parser nun wie
folgt geschrieben werden:

    (def abc1
      (parser-comb S
                   [S (cat (look (cat A (string "c")))
                           (plus (string "a")) B)
                    A (cat (string "a") (opt A)
                           (string "b"))
                    B (cat (string "b") (opt B)
                           (string "c"))]))

Die Resultate von Aufrufen von `abc` und `abc1` sind identisch. Der
Ausdruck\
`(abc1 "aaabbbccc")` liefert

    [:S "a" "a" "a" [:B "b" [:B "b" [:B "b" "c"] "c"] "c"]]

Zusammenfassung und Ausblick
============================

Die Implementierung von InstaComb mit der hier geschilderten
Funktionalität erforderte weniger als 250 Zeilen Clojure-Code. Das liegt
in erster Linie daran, dass das eigentliche Parsen durch Instaparse
bereit gestellt und hier nur wiederverwendet wird und der Zugriff auf
die einem Instaparse-Parser zugrunde liegende Grammatik in einfacher
Weise möglich war. Damit war die Voraussetzung für eine einfache
Implementierung der InstaComb-Funktionen gegeben.

Dass für rekursive Regeln ein Macro (`parser-comb`) benutzt werden muss,
stellt eine gewisse Unsymmetrie in der Benutzung von InstaComb dar. Da
Clojure aber bei Funktionsanwendungen mit applikativer Auswertung
arbeitet, können nur Argumente übergeben werden, die auch vor der
Funktionsanwendung auswertbar sind. Das wäre bei rekursiven Regeln nicht
der Fall. Um diese Unsymmetrie zu beseitigen, könnte man natürlich alle
InstaComb-Funktionen durch Macros ersetzen. Ob diese Alternative die
Benutzung von InstaComb erleichterte, bliebe noch zu untersuchen.

Instaparse ist nicht nur für nahezu jede Art von kontextfreier Grammatik
einsetzbar, sondern liefert auch für den Fall, dass eine Eingabe nicht
der Grammatik folgt, präzise Fehlermeldungen. Diese Funktionalität
bleibt bei InstaComb erhalten.

Instaparse stellt außerdem Funktionen bereit, die den Parsebaum zu
transformieren und zu visualisieren erlauben. Diese Funktionen stehen
derzeit in InstaComb noch nicht zur Verfügung.

[^1]: domain specific language

[^2]: Meist handelt es sich hierbei um eine universelle
    Programmiersprache.

[^3]: ​s. <http://pharo.org> oder @Pharo2009

[^4]: ​s. <http://clojure.org> oder @AmitRathore2012

[^5]: ​s. <http://de.wikipedia.org/wiki/Backus-Naur-Form>

[^6]: entnommen aus <https://github.com/Engelberg/instaparse>

[^7]: `parser` ist innerhalb von Instaparse dem Namespace `insta`
    zugeordnet.

[^8]: eine der Standard-Datenstrukturen in Clojure

[^9]: Namen mit vorangestelltem Dopplelpunkt

[^10]: vgl. Abschnitt [sec:parserkombinatoren]

[^11]: <http://www.antlr.org>

[^12]: <https://javacc.java.net>

[^13]: <http://www.eclipse.org>

[^14]: <https://eclipse.org/Xtext/>

[^15]: <https://www.haskell.org/haskellwiki/Parsec>

[^16]: <https://github.com/petitparser/java-petitparser>

[^17]: [http://www.scala-lang.org/api/2.10.2/index.html\#scala.util.parsing.combinator.Parsers](http://www.scala-lang.org/api/2.10.2/index.html#scala.util.parsing.combinator.Parsers)

[^18]: <https://github.com/youngnh/parsatron>

[^19]: <https://github.com/van-clj/zetta-parser>

[^20]: Hier und im Folgenden wird davon ausgegangen das `c` den
    Clojure-Namespace `instaparse.combinators` bezeichnet, der die
    Instparse-Kombinatorfunktionen enthält.

[^21]: `(`terminal? p) prüft, ob `p` ein terminales Symbol erkennt;
    Terminale sind Strings und reguläre Ausdrücke.

[^22]: Die Kombinatorfunktionen von Instaparse sind hier mit dem
    vollständigen Namespace-Pfad `instaparse.combinators` qualifiziert.
